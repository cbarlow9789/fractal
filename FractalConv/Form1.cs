﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace FractalConv
{
    public partial class Form1 : Form
    {

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;
            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public float H
            {
                get { return h; }
            }
            public float S
            {
                get { return s; }
            }
            public float B
            {
                get { return b; }
            }
            public int A
            {
                get { return a; }
            }
            public Color Color
            {
                get
                {
                    return FromHSB1(this);
                }
            }

            public static Color FromHSB1(HSBColor hsbColor) //Sets the fractal colour to red
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = min;
                        g = h * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h < 180f)
                    {
                        r = max;
                        g = min;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = max;
                        g = -(h - 240f) * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h <= 360f)
                    {
                        r = min;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 255;
                        g = 255;
                        b = 255;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

            public static Color FromHSB(HSBColor hsbColor) //Sets the fractal colour to blue
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = max;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

            
            public static Color FromHSB2(HSBColor hsbColor)     //Sets the fractal colour to green
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = 0;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = 0;
                    }
                    else if (h < 180f)
                    {
                        r = 0;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = 0;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = 0;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

        public Form1()
        {
            InitializeComponent();
            init();
            start();
        }

        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        private Bitmap picture;
        private Graphics g1;
        private Cursor c1, c2;
        private HSBColor HSBcol = new HSBColor();
        private bool _mousePressed;

        public void init() // all instances will be prepared
        {
            HSBcol = new HSBColor();
            finished = false;
            x1 = Size.Width;
            y1 = Size.Height;
            xy = (float)x1 / (float)y1;
            picture = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture);
            finished = true;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = ".BMP file (*.bmp)|*.bmp";     //Limit file save to only .bmp
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                picture.Save(saveFileDialog1.FileName);     //If user clicks ok to save, save image
            }

        }

        private void saveStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string xstartState = xstart.ToString();     //Convert variables into strings
            string ystartState = ystart.ToString();
            string yendeState = yende.ToString();
            string xendeState = xende.ToString();


            string[] lines = { xstartState, ystartState, yendeState, xendeState };      //Save strings into array
            
            File.WriteAllLines(@"C:\Users\conor\Documents\State.txt", lines);       //Write array to file
            MessageBox.Show("Program State Saved", "State", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);     //Show message box showing file is saved
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            mandelbrot();       //Rerun mandlebrot if colour changed
            this.Refresh();
        }

        private void deleteStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(@"C:\Users\conor\Documents\State.txt"))     // If state file exists
            {
                File.Delete(@"C:\Users\conor\Documents\State.txt");     //Delete state file
                MessageBox.Show("Program State Deleted", "State", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);   //Show message box showing file is deleted
                Application.Exit();
            }
            else
            {
                MessageBox.Show("No state saved", "State", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);      //Show messagebox showing no state exists

            }
        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        public void Fractal_Paint(object sender, PaintEventArgs e)
        {
  
            Graphics g = e.Graphics;
            update(g);
        }


        private void update(Graphics g)
        {

            g.DrawImage(picture, 0, 0, 640, 480);
            Color color = (Color.LawnGreen);
            Pen myPen = new Pen(color, 2);

            if (rectangle)
            {

                if (xs < xe)
                {
                    if (ys < ye) g.DrawRectangle(myPen, xs, ys, (xe - xs), (ye - ys));
                    else g.DrawRectangle(myPen, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g.DrawRectangle(myPen, xe, ys, (xs - xe), (ye - ys));
                    else g.DrawRectangle(myPen, xe, ye, (xs - xe), (ys - ye));
                }
            }
        }
        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;

            action = false;

            Color color = Color.Black;
            Pen myPen = new Pen(color);
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                        if (toolStripComboBox1.SelectedIndex == 1)
                        {
                            color = HSBColor.FromHSB1(new HSBColor(h * 255, 0.8f * 255, b * 255)); //BLUE
                        }else if (toolStripComboBox1.SelectedIndex == 2)
                        {
                            color = HSBColor.FromHSB2(new HSBColor(h * 255, 0.8f * 255, b * 255)); //GREEN
                        }else
                        {
                            color = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255)); //RED
                        }
                        myPen = new Pen(color);
                        alt = h;
                    }

                    g1.DrawLine(myPen, x, y, x + 1, y);
                }

            action = true;
        }
        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            if (File.Exists(@"C:\Users\conor\Documents\State.txt"))     //If a state file is saved
            {
                string[] lines2 = System.IO.File.ReadAllLines(@"C:\Users\conor\Documents\State.txt");       //Read values from file
                if (lines2[0] == "true")
                {
                    xstart = Convert.ToDouble(lines2[0]);       //Convert all strings to doubles
                    ystart = Convert.ToDouble(lines2[1]);
                    yende = Convert.ToDouble(lines2[2]);
                    xende = Convert.ToDouble(lines2[3]);
                }
            }
            else
            {
                xstart = SX;
                ystart = SY;
                xende = EX;
                yende = EY;
            }
                if ((float)((xende - xstart) / (yende - ystart)) != xy)
                    xstart = xende - (yende - ystart) * (double)xy;
            }

        public void mousePressed(object sender, MouseEventArgs e)
        {
            
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        }


        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            _mousePressed = true;
            xs = e.X;
            ys = e.Y;

        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            _mousePressed = false;
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                this.Refresh();
            }


        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (_mousePressed)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                this.Refresh();
            }

        }

        public String getAppletInfo()
        {
            return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
        }
    }
}